package ro.deloitte.digital.jhipster.tickets.domain.enumeration;

/**
 * The TicketType enumeration.
 */
public enum TicketType {
    BLIND, EARLY_BIRD, REGULAR, VIP
}
